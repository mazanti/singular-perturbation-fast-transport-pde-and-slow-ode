# -*- coding: utf-8 -*-
"""
Simulation code used for the numerical illustration of the article "Frequency
domain approach for the stability analysis of a fast hyperbolic PDE coupled
with a slow ODE" by Gonzalo Arias, Swann Marx, and Guilherme Mazanti.
Code written by Guilherme Mazanti.
We refer to the article for details of the system being simulated here.
Distributed under the GPL v3 license.
"""

import numpy as np
import scipy.linalg as la
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.integrate import simpson

plt.close("all")
plt.rcParams.update({
  "mathtext.fontset": "stix"
})

CASE = 1
# CASE = 1 means that we will run the simulation for epsilon = 0.01, 0.03,
# 0.05, 0.1, 0.125, and show three graphs: solution of the ODE, trace of the
# solution of the PDE at x = 0, and solution of the PDE for epsilon = 0.01.
#
# CASE = 2 means that we will run the simulation for several values of epsilon
# and trace the norm of the difference between the simulated values and the
# approximation given by singular perturbation.

def simul_ros(Ar, z0, T, Nt):
  """
  Simulation of the reduced order system.

  Parameters
  ----------
  Ar : numpy.ndarray, square matrix with shape (n, n)
    Matrix of the reduced order system.
  z0 : numpy.ndarray, vector with shape (n,)
    Initial condition.
  T : positive number
    Time horizon.
  Nt : positive integer
    Number of time steps.

  Returns
  -------
  t : numpy.ndarray, vector of shape (Nt,)
    Times.
  z_ros : numpy.ndarray, shape (Nt, n)
    Values of the variable of the reduced order system.
  """
  t = np.linspace(0, T, Nt)
  z_ros = la.expm(t.reshape(-1, 1, 1) * Ar.reshape(1, *Ar.shape)).dot(z0)
  return t, z_ros

def simul_bls(Lambda, G1, y0, T, Nt, CFL):
  """
  Simulation of the boundary layer system.

  Parameters
  ----------
  Lambda : numpy.ndarray, vector with shape (m,)
    Velocities of each component of the state vector.
  G1 : numpy.ndarray, square matrix with shape (m, m)
    Matrix determining the boundary condition.
  y0 : function: x numpy.ndarray (Nx,) |--> y0(x) numpy.ndarray (Nx, m)
    Function determining the initial condition. Should accept as inputs vectors
    of any size Nx. This is the initial condition of the boundary layer system
    (i.e., after the transformation).
  T : positive number
    Time horizon.
  Nt : positive integer
    Number of time steps.
  CFL : number in the interval (0, 1]
    Desired CFL condition. The true CFL condition will be smaller than or equal
    to this value

  Returns
  -------
  t : numpy.ndarray, vector of shape (Nt,)
    Times.
  x : numpy.ndarray, vector of shape (Nx,)
    Space variables.
  y_bls : numpy.ndarray, shape (Nt, Nx, m)
    Values of the variable of the boundary layer system.
  """
  t = np.linspace(0, T, Nt)
  dt = t[1] - t[0]
  dx = Lambda.max() * dt / CFL
  Nx = int(np.floor(1/dx)) + 1
  x = np.linspace(0, 1, Nx)
  dx = x[1] - x[0]
  CFL = Lambda.max() * dt / dx

  m = Lambda.size

  y_bls = np.empty((Nt, Nx, m))
  y_bls[0, :, :] = y0(x)

  for n in range(Nt - 1):
    y_bls[n+1, 1:, :] = (np.ones(m) - dt / dx * Lambda).reshape(1, m) * y_bls[n, 1:, :] + (dt / dx * Lambda).reshape(1, m) * y_bls[n, :-1, :]
    y_bls[n+1, 0, :] = G1.dot(y_bls[n+1, -1, :])

  return t, x, y_bls

def simul_full(A, B, Lambda, G1, G2, z0, y0, epsilon, T, Nt, CFL):
  """
  Simulation of the full system.

  Parameters
  ----------
  A : numpy.ndarray, square matrix with shape (n, n)
    Matrix of the ODE.
  B : numpy.ndarray, shape (n, m)
    Matrix in the ODE determining the coupling with the PDE.
  Lambda : numpy.ndarray, vector with shape (m,)
    Velocities of each component of the state vector of the PDE.
  G1 : numpy.ndarray, square matrix with shape (m, m)
    Matrix determining the boundary condition of the PDE.
  G2 : numpy.ndarray, shape (m, n)
    Matrix in the boundary condition of the PDE determiing the coupling with
    the ODE.
  z0 : numpy.ndarray, vector with shape (n,)
    Initial condition of the ODE.
  y0 : function: x numpy.ndarray (Nx,) |--> y0(x) numpy.ndarray (Nx, m)
    Function determining the initial condition of the PDE. Should accept as
    inputs vectors of any size Nx.
  epsilon : positive number
    Parameter of the singular perturbation.
  T : positive number
    Time horizon.
  Nt : positive integer
    Number of time steps.
  CFL : number in the interval (0, 1]
    Desired CFL condition. The true CFL condition will be smaller than or equal
    to this value.

  Returns
  -------
  t : numpy.ndarray, vector of shape (Nt,)
    Times.
  x : numpy.ndarray, vector of shape (Nx,)
    Space variables.
  z : numpy.ndarray, shape (Nt, n)
    Values of the variable of the ODE.
  y : numpy.ndarray, shape (Nt, Nx, m)
    Values of the variable of the PDE.
  """
  t = np.linspace(0, T, Nt)
  dt = t[1] - t[0]
  dx = Lambda.max() / epsilon * dt / CFL
  Nx = int(np.floor(1/dx)) + 1
  x = np.linspace(0, 1, Nx)
  dx = x[1] - x[0]
  CFL = Lambda.max() / epsilon * dt / dx

  n = A.shape[0]
  m = Lambda.size


  z = np.empty((Nt, n))
  y = np.empty((Nt, Nx, m))

  z[0, :] = z0
  y[0, :, :] = y0(x)

  for n in range(Nt - 1):
    z[n+1, :] = z[n, :] + dt * (A.dot(z[n, :]) + B.dot(y[n, -1, :]))
    y[n+1, 1:, :] = (np.ones(m) - dt * Lambda / dx / epsilon).reshape(1, m) * y[n, 1:, :] + (dt * Lambda / dx / epsilon).reshape(1, m) * y[n, :-1, :]
    y[n+1, 0, :] = G1.dot(y[n+1, -1, :]) + G2.dot(z[n+1, :])

  return t, x, z, y

# =============================================================================
# Parameters of the systems
# =============================================================================
A = np.array([[2]])

G1 = np.array([[1, -2], [1/4, -1/2]])
Lambda = np.array([1, 1/2])

B = np.array([[1, 2]])
G2 = np.array([[-1], [0]])

Gr = la.inv(np.eye(2) - G1).dot(G2)
Ar = A + B.dot(Gr)

z0 = np.array([1])
y0 = lambda x: G2.dot(z0).reshape(1, -1) * np.cos(5*np.pi/2 * x).reshape(-1, 1)
y0_bls = lambda x: y0(x) - Gr.dot(z0)

if CASE == 1:
  epsilons = np.array([0.01, 0.03, 0.05, 0.1, 0.125])
else:
  epsilons = np.logspace(-3, -1, 9)


# =============================================================================
# Parameters of the simulations
# =============================================================================
T = 5
T_bls = 20

CFL = 0.9

Nt_ros = 1000
Nt_epsilon = 60 # Number of time steps in each interval of size epsilon
Nt_bls = T_bls * Nt_epsilon
Nts = (Nt_epsilon * T / epsilons + 1).astype(int)
# When epsilon is small, we need very small time steps in order to have enough
# space steps in the discretization of the PDE while ensuring a reasonable CFL
# condition for stability

# =============================================================================
# Simulations
# =============================================================================
print("Computing solution of the reduced order system in the time interval [0, {}] with {} time steps...".format(T, Nt_ros), end = " ")
t_ros, z_ros = simul_ros(Ar, z0, T, Nt_ros)
print("ok!")

print("Computing the solution of the boundary layer system in the time interval [0, {}] with {} time steps...".format(T_bls, Nt_bls), end = " ")
t_bls, x_bls, y_bls = simul_bls(Lambda, G1, y0_bls, T_bls, Nt_bls, CFL)
print("ok!")

res_full = []
for i in range(epsilons.size):
  print("Computing the solution of the full system with epsilon = {} in the time interval [0, {}] with {} time steps...".format(epsilons[i], T, Nts[i]), end = " ")
  res_full.append(simul_full(A, B, Lambda, G1, G2, z0, y0, epsilons[i], T, Nts[i], CFL))
  print("ok!")

# =============================================================================
# Plots
# =============================================================================
linestyles = [(0, (5, 1)), (0, (4, 1, 1, 1)), (0, (3, 1, 1, 1, 1, 1)), (0, (2, 1, 1, 1, 1, 1, 1, 1)), (0, (1, 1))]
if CASE == 1:
  fig, ax = plt.subplots(figsize = (5, 5))
  ax.grid(True)
  ax.set_axisbelow(True)
  ax.set_xlabel("Time $t$")
  ax.set_ylabel("$z(t)$")
  ax.set_title("Solution of the ODE")
  ax.plot(t_ros, z_ros, label = "Reduced order system")
  for i in range(epsilons.size):
    t, _, z, _ = res_full[i]
    ax.plot(t, z, linestyle = linestyles[i], label = r"$\varepsilon = " + "{:.3g}".format(epsilons[i]) + "$")
  ax.legend()

  fig, ax = plt.subplots(2, 1, sharex = True, figsize = (5, 9))
  y_ros = Gr.dot(z_ros.T)
  for i in range(2):
    ax[i].grid(True)
    ax[i].set_axisbelow(True)
    ax[i].set_ylabel("$y_{" + str(i+1) + r"}(0, \cdot)$")
    ax[i].plot(t_ros, y_ros[i, :], label = r"$\bar{y}_\ast(\cdot)$")
  ax[-1].set_xlabel("Time")
  fig.suptitle("Trace of the solution of the PDE at x = 0")
  for i in range(epsilons.size):
    t, _, _, y = res_full[i]
    for j in range(2):
      ax[j].plot(t, y[:, 0, j], linestyle = linestyles[i], label = r"$\varepsilon = " + "{:.3g}".format(epsilons[i]) + "$")
  for i in range(2):
    ax[i].legend()

  fig, ax = plt.subplots(2, 2, subplot_kw={"projection": "3d"}, figsize = (9, 9))
  i = 0
  ax[0, 0].set_position([0, 0.5, 0.45, 0.45])
  ax[1, 0].set_position([0, 0.03, 0.45, 0.45])
  ax[0, 1].set_position([0.5, 0.5, 0.45, 0.45])
  ax[1, 1].set_position([0.5, 0.03, 0.45, 0.45])
  fig.suptitle(r"Solution of the PDE with $\varepsilon = " + "{:3g}$".format(epsilons[i]))
  t, x, _, y = res_full[i]
  y_fast = y[t <= 8*epsilons[i], :, :]
  t_fast = t[t <= 8*epsilons[i]]
  T_fast, X_fast = np.meshgrid(t_fast / epsilons[i], x)
  T, X = np.meshgrid(t, x)
  for i in range(2):
    for j in range(2):
      ax[i, j].set_xlabel("$t$")
      ax[i, j].set_ylabel("$x$")
      ax[i, j].set_zlabel("$y_{}(\cdot, \cdot)$".format(i+1))
    ax[i, 0].plot_surface(T_fast, X_fast, y_fast[:, :, i].T, rcount = 50, ccount = 50, cmap = "plasma", antialiased=False, linewidth = 0)
    ax[i, 1].plot_surface(T, X, y[:, :, i].T, rcount = 100, ccount = 100, cmap = "plasma", antialiased=False, linewidth = 0)
    ticks = np.arange(0, 10, 2)
    ax[i, 0].set_xticks(ticks, ["${:1g}".format(t) + r"\varepsilon$" if t != 0 else "$0$" for t in ticks])
    # rcount and ccount should be increased to get a better quality of the graphs. In the article, we took rcount = ccount = 1000 for the plot of y_fast and rcount = ccount = 5000 for the plot of y. With these values, the graph may take a long time to be shown.

else:
  fig, ax = plt.subplots(figsize = (5, 5))
  ax.grid(True)
  ax.set_axisbelow(True)
  ax.set_xlabel(r"$\varepsilon$")
  ax.set_xscale("log")
  ax.set_ylabel(r"$\Vert z(\cdot) - \bar{z}(\cdot)\Vert_{L^2(0, T; \mathbb{R})}$")
  ax.set_yscale("log")
  ax.set_title("Difference between ODE and reduced order system")
  norms_z = np.empty(epsilons.size)
  for i in range(epsilons.size):
    t, x, z, y = res_full[i]
    norms_z[i] = np.sqrt(simpson(((interp1d(t, z, axis = 0)(t_ros) - z_ros) ** 2).sum(1), t_ros))
  ax.plot(epsilons, norms_z)
# Singular perturbation – Fast transport PDE and slow ODE

This repository contains simulation code used for the numerical illustration of the article &ldquo;Frequency domain approach for the stability analysis of a fast hyperbolic PDE coupled with a slow ODE&rdquo; by Gonzalo Arias, Swann Marx, and Guilherme Mazanti. We refer to the article for details of the system being simulated here. This code is distributed under the GPL v3 licence (see the file LICENSE.md).
